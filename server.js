import express from 'express';
import bodyParser from 'body-parser';
import DB from './config/db';
import UsersController from './controllers/users';

let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( {extended: true} ));

app.get('/users', UsersController.all);
app.get('/users/:id', UsersController.findById);
app.post('/users', UsersController.create);
app.put('/users/:id', UsersController.update);
app.delete('/users/:id', UsersController.delete);

let db = new DB;
if (db != null) {
    app.listen(3000, () => {
        console.log('Server started on 3000 port');
    });
}
