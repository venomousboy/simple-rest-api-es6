let MongoClient = require('mongodb').MongoClient;

let instance = null;

export default class DB { 

    constructor () 
    {
        if(!instance) {
            let url = 'mongodb://localhost:27017/db';
            MongoClient.connect(url, (err, db) => {
                if (err) {
                    return err;
                }
                instance = db;
            });
        }
        return instance;
      }
}

