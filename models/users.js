let ObjectID = require('mongodb').ObjectID;
import DB from '../config/db';

export default class UsersModel {
    
    constructor () 
    {
        let db = new DB();
        this._model = db.collection('users');
    }
    
    async all ()
    {
        return await this._model.find().toArray();
    }

    async findById (id)
    {
        return await this._model.findOne({_id: ObjectID(id)});
    }
    
    async create (user)
    {
        return await this._model.insert(user);
    }

    async update (id, data)
    {
        return await this._model.updateOne({_id: ObjectID(id)}, data);
    }

    async delete (id)
    {
        return await this._model.deleteOne({_id: ObjectID(id)});
    }
}