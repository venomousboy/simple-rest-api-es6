import UsersModel from '../models/users';

export default class UsersController {
    
    static async all (req, res)
    {
        try {
            let model = new UsersModel;
            let users = await model.all();
            res.send(users);        
        } catch (err) {
            console.log(err);
            return res.sendStatus(500);
        }
    }
    
    static async findById (req, res)
    {
        try {
            let model = new UsersModel;
            let user = await model.findById(req.params.id);
            res.send(user);        
        } catch (err) {
            console.log(err);
            return res.sendStatus(500);
        }
    }

    static async create (req, res)
    {
        try {
            let user = {
                name: req.body.name,
                lastname: req.body.lastname
            };

            let model = new UsersModel;
            let result = await model.create(user);
            res.send(result);        
        } catch (err) {
            console.log(err);
            return res.sendStatus(500);
        }
    }

    static async update (req, res)
    {
        try {
            let model = new UsersModel;
            let user = await model.update(req.params.id, 
            {
                name: req.params.name,
                lastname: req.params.lasname 
            });
            return res.sendStatus(200);        
        } catch (err) {
            console.log(err);
            return res.sendStatus(500);
        }
    }

    static async delete (req, res)
    {
        try {
            let model = new UsersModel;
            let user = await model.delete(req.params.id);
            return res.sendStatus(200);        
        } catch (err) {
            console.log(err);
            return res.sendStatus(500);
        }
    }

}